import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router  } from '@angular/router';

import { ApiService } from '../services/api.service';
import { Repository} from '../models/repository.model';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})

export class ResultsComponent implements OnInit {
  readonly organizationParameterName = 'organization';
  originalOrginizationRepositories: Repository[];
  organizationName: string;
  organizationRepositories: Repository[];
  filterLanguages: string[];

  constructor(private route: ActivatedRoute, private apiService: ApiService, private router: Router) {
    this.route.queryParams.subscribe(params => {
      this.organizationName = params[this.organizationParameterName];
    });
    this.organizationRepositories = new Array<Repository>();
    this.filterLanguages = new Array<string>();
  }

  ngOnInit() {
    if (this.HasValidOrganizationName()) {
      this.SearchForOrganizationRepositories();
    }
  }

  private SearchForOrganizationRepositories() {
    this.apiService.getOrganizationRepositories(this.organizationName).subscribe((data) => {
      for (const repo of (data as any)) {
        const languages: string[] = this.SearchForRepositoryLanguages(repo.name);
        this.organizationRepositories.push(new Repository(repo.name, repo.stargazers_count, languages));
      }
    });
    this.originalOrginizationRepositories = this.organizationRepositories;
  }

  public SearchForRepositoryLanguages(repoName: string): string[] {
    const result = new Array<string>();
    this.apiService.getRepositoryLanguages(this.organizationName, repoName).subscribe((data) => {
      for (const language of Object.keys(data as any)) {
        result.push(language);

        if (!this.filterLanguages.includes(language)) {
          this.filterLanguages.push(language);
        }
      }
    });
    return result;
  }

  public HasValidOrganizationName() {
    let result = false;
    if (this.organizationName !== undefined) {
      if (this.organizationName.length > 0) {
        result = true;
      }
    }
    return result;
  }

  public HasValidRepositories() {
    let result = false;
    if (this.organizationRepositories !== undefined || this.organizationRepositories === null) {
      if (this.organizationRepositories.length > 0) {
        result = true;
      }
    }
    return result;
  }

  public ReturnHome() {
    this.router.navigate(['']);
  }

  public ViewRepoBranches(repoName: string) {
    this.router.navigate(['/results/branches'], {queryParams: {organization: this.organizationName, repository: repoName}});
  }

  public SortByStargazers(ascending: boolean) {
    if (ascending) {
      this.organizationRepositories = this.organizationRepositories.sort((repoA: Repository, repoB: Repository) =>
        repoA.countStargazers - repoB.countStargazers);
    } else {
      this.organizationRepositories = this.organizationRepositories.sort((repoA: Repository, repoB: Repository) =>
        repoB.countStargazers - repoA.countStargazers);
    }
  }

  public FilterByLanguage(language: string) {
    this.organizationRepositories = this.originalOrginizationRepositories;
    if (language !== '') {
      this.organizationRepositories = this.organizationRepositories.filter(repo => repo.languages.includes(language));
    }
  }
}
