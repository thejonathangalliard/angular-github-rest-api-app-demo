import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { ApiService } from './api.service';


describe('ApiService', () => {

      beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [ApiService]
      }));

      it('should be created', () => {
        const service: ApiService = TestBed.get(ApiService);
        expect(service).toBeTruthy();
      });

      it('should have getOrganizationRepositories function', () => {
        const service: ApiService = TestBed.get(ApiService);
        expect(service.getOrganizationRepositories).toBeTruthy();
      });

      it('should have getRepositoryBranches function', () => {
        const service: ApiService = TestBed.get(ApiService);
        expect(service.getRepositoryBranches).toBeTruthy();
      });

      it('should have getRepositoryLanguages function', () => {
        const service: ApiService = TestBed.get(ApiService);
        expect(service.getRepositoryLanguages).toBeTruthy();
      });

    });


