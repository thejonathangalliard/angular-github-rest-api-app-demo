import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GITHUB_API_URL } from '../constants';
import { Repository } from '../models/repository.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  public getOrganizationRepositories(organization: string) {
    const data = this.httpClient.get(GITHUB_API_URL + '/orgs/' + organization + '/repos');
    return data;
  }

  public getRepositoryBranches(owner: string, repositoryName: string) {
    const data = this.httpClient.get(GITHUB_API_URL + '/repos/' + owner + '/' + repositoryName + '/branches');
    return data;
  }

  public getRepositoryLanguages(owner: string, repositoryName: string) {
    const data = this.httpClient.get(GITHUB_API_URL + '/repos/' + owner + '/' + repositoryName + '/languages');
    return data;
  }
}
