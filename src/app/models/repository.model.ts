export class Repository {
    name: string;
    countStargazers: number;
    languages: string[];

    constructor(name: string, countStargazers: number, languages?: string[]) {
        this.name = name;
        this.countStargazers = countStargazers;
        this.languages = languages || new Array<string>();
    }
}
