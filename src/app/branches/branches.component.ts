import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router  } from '@angular/router';

import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css']
})
export class BranchesComponent implements OnInit {
  readonly organizationParameterName = 'organization';
  readonly repositoryParameterName = 'repository';

  organizationName: string;
  repositoryName: string;
  repositoryBranches: any;

  constructor(private route: ActivatedRoute, private apiService: ApiService, private router: Router) {
    this.route.queryParams.subscribe(params => {
      this.organizationName = params[this.organizationParameterName];
      this.repositoryName = params[this.repositoryParameterName];
    });
  }

  ngOnInit() {
    if (this.HasValidOrganizationName() && this.HasValidRepositoryName()) {
      this.SearchForRepositoryBranches();
    }
  }

  private SearchForRepositoryBranches() {
    this.apiService.getRepositoryBranches(this.organizationName, this.repositoryName).subscribe((data) => {
      this.repositoryBranches = data;
    });
  }

  public HasValidOrganizationName() {
    let result = false;
    if (this.organizationName !== undefined) {
      if (this.organizationName.length > 0) {
        result = true;
      }
    }
    return result;
  }

  public HasValidRepositoryName() {
    let result = false;
    if (this.repositoryName !== undefined) {
      if (this.repositoryName.length > 0) {
        result = true;
      }
    }
    return result;
  }

  public HasValidBranches() {
    let result = false;
    if (this.repositoryBranches !== undefined) {
      result = true;
    }
    return result;
  }

  public ReturnToRepoResults() {
    this.router.navigate(['../results'], {queryParams: {organization: this.organizationName}});
  }

  public ReturnHome() {
    this.router.navigate(['']);
  }
}
